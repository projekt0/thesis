using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Lean.Touch;

/// <summary>This component allows you to spawn a prefab at the specified world point.
/// NOTE: To trigger the prefab spawn you must call the Spawn method on this component from somewhere.</summary>
[HelpURL(LeanTouch.HelpUrlPrefix + "LeanSpawn")]
[AddComponentMenu(LeanTouch.ComponentPathPrefix + "Handle")]
public class LeanSpawn : MonoBehaviour
{



    /// <summary>This will spawn <b>Prefab</b> at the current <b>Transform.position</b>.</summary>
    public void Spawn(int vale, Transform Cube, Transform Sphere, Transform Plane)
    {
        if (vale == 0)
        {
            Spawn(transform.position, Cube);
        }
        else if (vale == 1)
        {
            Spawn(transform.position, Sphere);
        }
        else if (vale == 2)
        {
            Spawn(transform.position, Plane);
        }
    }

    /// <summary>This will spawn <b>Prefab</b> at the specified position in world space.</summary>
    public void Spawn(Vector3 position, Transform Prefab)
    {
        if (Prefab != null)
        {
            var clone = Instantiate(Prefab);

            clone.position = position;

            clone.gameObject.SetActive(true);
        }
    }


}