using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveWL : MonoBehaviour
{

    public void Start(){

        }

        public void toggle(){
          int modelPosition = LayerMask.NameToLayer("WeightLoad");
           int modelMask = 1 << modelPosition;


           bool isLayerActive = (Camera.main.cullingMask & modelMask) != 0;

           if (!isLayerActive) {
             Camera.main.cullingMask |= modelMask;
           } else {
             Camera.main.cullingMask &= ~ modelMask;
             }
        }
}
