﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Reconnector : MonoBehaviour
{
    public const string PROXY = "192.168.1.29:8080"; //Netzwerk
    //public const string PROXY = "fepc-win002.dev.kbee.lan:7070"; //Netzwerk
    public InputField iP;
    public InputField name;
    public InputField pass;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(iP.name);
        Debug.Log(name.name);
        Debug.Log(pass.name);
    }

    // Update is called once per frame
    void Update()
    {

    }
    //TODO OnError von Connection Robot aufrufen und mit find with tag aufrufen um GUI meldung auszugeben

    public void ReConnect(){
        Debug.Log("Reconnect");
        string iPS = iP.text;
        string nameS = name.text;
        string passS = pass.text;

        string urlNew = "ws://" + PROXY + "/desk/api/robot/configuration?user=" + nameS + "&pass=" + passS + "&host=" + iPS;

        GameObject go = GameObject.FindWithTag("Panda");
        if (go == null) {
          Debug.LogWarning("Could not find Gameobject with Tag 'Panda', Check if it exists and the tag is correct.");
          return;
        }

        ConnectionRobot panda = go.GetComponent<ConnectionRobot>(); //TODO Button Disable if panda not found
        if (panda == null) {
          Debug.LogWarning("Could not find Component 'ConnectionRobot' beneath robot " + go.name + ", Check if it does exist on the gameobject");
          return;
        }

          panda.enabled = false;
          panda.url = urlNew;
          panda.enabled = true;


    }
}
