#!/usr/bin/env python

"""
Simple Websocket Server for publishing "fake" Robot state data

Installation:
* pip install --user websockets
* pip install --user numpy

"""
import math as m
import time
import asyncio
import websockets
import json
import numpy as np

payload = {
    'jointAngles': [0] * 7,     #  All joint position values in [rad]
    'cartesianPose': np.diag([1,1,1,1]).flatten().tolist(),  # 4x4 column major transformation matrix
    'estimatedTorques': [0] * 7,     # All sensed external joint torques [Nm]
    'estimatedForces': [0] * 6       # Estiamted sensed external forces[0..2] & torques[3..5] at EE [N/Nm]
}


def control(t):
    f = 0.2  # [Hz]
    payload['jointAngles'][1] = -0.1
    payload['jointAngles'][3] = m.pi/2. * m.sin(2*m.pi*t*f)
    payload['jointAngles'][6] = m.pi/4

    payload['estimatedTorques'][1] = 17 if 2 < t and t < 10 else 0


async def hello(websocket, path):

    f = 10   # [Hz]
    t = 0

    while True:
        control(t)
        print(json.dumps(payload))
        await websocket.send(json.dumps(payload))
        time.sleep(1./f)
        t += 1./f

start_server = websockets.serve(hello, "0.0.0.0", 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
