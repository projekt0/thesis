using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System;
using System.Net;
using System.IO;
using System.Threading;
using System.Reflection;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

using WebSocketSharp;
//using UnityEngine.WSA;

using System.Text.RegularExpressions;

public class ConnectionRobot : MonoBehaviour
{
public string url = "ws://fepc-win002.dev.kbee.lan:7070/desk/api/robot/configuration?user=franka&pass=franka123&host=192.168.0.3";
public Transform[] joints = new Transform[7];
public Transform[] weight = new Transform[7];
public Gradient TorqueToColor;
public AnimationCurve TorqueToRadius;
WebSocket socket;
private RobotState robotState;

public bool Connected {
        get; private set;
}

// Start is called before the first frame update
void OnEnable()
{

        socket = new WebSocket(url);
        socket.OnOpen += Socket_OnConnect;
        socket.OnError += Socket_OnError;
        socket.OnMessage += Socket_OnMessage;
        socket.OnClose += Socket_OnClose;

  socket.Connect();

}

void OnDisable()
{
    if(socket == null){
      return;
    }

    //ConnectionRobot component = GetComponent<...>()
    //component.enabled = false;
    //component.url = textfeld.getText()
    //comnpoent.enabled = true;

    socket.OnError -= Socket_OnError;
    socket.OnMessage -= Socket_OnMessage;
    socket.Close();
}

void Socket_OnConnect(object sender, EventArgs e){

  Debug.Log("Connected to Script " + url);
  Connected = true;

}

void Socket_OnClose(object sender, WebSocketSharp.CloseEventArgs e){
  Connected = false;
}

public void set_config(List<float> jointAngles, List<float> estimatedTorques){
        try{
                if(joints.Length != 7) {
                        Debug.LogWarning("Joints off is expected to be 7 but its not");
                }

                if(jointAngles.Count != 7) {
                        Debug.LogWarning("Received Message contained Joint Angles Joints of length > 7");
                }



              //  Debug.Log(jointAngles[1] * Mathf.Rad2Deg);
                for(int i = 0; i < jointAngles.Count; i++) {
                        var angles = joints[i].localEulerAngles;
                        angles.z = -1* jointAngles[i] * Mathf.Rad2Deg;
                        joints[i].localEulerAngles = angles;
                }

                for(int i = 0; i < estimatedTorques.Count; i++){
                  var forces = estimatedTorques[i];
                  forces = forces / 10;
                  float radius = TorqueToRadius.Evaluate(forces);
                  Color color = TorqueToColor.Evaluate(forces);
                    MeshRenderer meshRenderer = weight[i].GetComponent<MeshRenderer>();
                    // change color
                    meshRenderer.material.color = color;
                }




        }
        catch(Exception e) {
                Debug.LogError(e.Message);
        }



}


private void Socket_OnMessage(object sender, MessageEventArgs e)
{
        var jsonString = System.Text.Encoding.Default.GetString(e.RawData);
        var robotState = JsonUtility.FromJson<RobotState>(jsonString);

        this.robotState = robotState;
}

private void Socket_OnError(object sender, WebSocketSharp.ErrorEventArgs e)
{
        Debug.LogError("Could not create websocket" + e.Message);
        //TODO OnError von Connection Robot aufrufen und mit find with tag aufrufen um GUI meldung auszugeben
}

// Update is called once per frame
void Update()
{

        if(robotState == null) return;
        set_config(robotState.jointAngles, robotState.estimatedTorques);

}


}

[Serializable]
public class RobotState {



public List<float> jointAngles;
public List<float> estimatedForces;
public List<float> estimatedTorques;


}
