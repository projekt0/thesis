﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Nav : MonoBehaviour
{

  [Range(0,1)]
  public float scale = 0.001f;
  public Vector3 direction;
  GameObject robot;

    public void move()
    {
      if(robot == null){
        return;
      }
      robot.transform.Translate(direction * 0.001f);
    }

    void Update(){

      if(robot == null){
        robot = GameObject.FindWithTag("Panda");
        return;
      }
      robot = robot.transform.Find("link0").gameObject;

    }
}
