using UnityEngine;
using UnityEngine.UI;

public class PositionText : MonoBehaviour
{
    public Text m_MyText;
     GameObject obj;

    void Start()
    {
        //Text sets your text to say this message
        m_MyText.text = "Position:";
    }

    void Update()
    {

      if(obj == null){
        obj = GameObject.FindWithTag("Panda");
          m_MyText.text = "Position: --";
        return;
      }
        //Press the space key to change the Text message

            m_MyText.text = "Position: \n"
             + obj.transform.Find("link0").gameObject.transform.position.ToString("f3");

    }
}
